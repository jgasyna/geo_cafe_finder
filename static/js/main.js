$(document).ready(function(){
    $('.login_button').on('click', function(event) {
        event.preventDefault();
         $.ajax({
            url: '/login',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                if (response.success){
                    location.reload();
                } else {
                    $('.login_error').text(response.message);
                }
            }
        });
    });

    $('.signup_button').on('click', function(event) {
        event.preventDefault();
         $.ajax({
            url: '/signup',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                if (response.success){
                    location.reload();
                } else {
                    $('.signup_error').text(response.message);
                }
            }
        });
    });

    $('.forgot_password_button').on('click', function(event) {
        $('#spinner').show();
        event.preventDefault();

         $.ajax({
            url: '/forgotPassword',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                $('#spinner').hide();
                if (response.success){
                    location.reload();
                } else {
                    $('.login_error').text(response.message);
                }
            }
        });
    });

    $('.forgot_username_button').on('click', function(event) {
        $('#spinner').show();
        event.preventDefault();

         $.ajax({
            url: '/forgotUsername',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                $('#spinner').hide();
                if (response.success){
                    location.reload();
                } else {
                    $('.login_error').text(response.message);
                }
            }
        });
    });

    $('.change_password_button').on('click', function(event) {
        $('#spinner').show();
        event.preventDefault();

         $.ajax({
            url: '/changePassword',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                $('#spinner').hide();
                if (response.success){
                    location.reload();
                } else {
                    $('.login_error').text(response.message);
                }
            }
        });
    });

    $("#addCommentButton").click(function () {
        $('#commentForm').show();
    });

     $("#forgotUsername").click(function () {
        $('#forgot_password_form').hide();
        $('#change_password_form').hide();
        $('#forgot_username_form').show();
    });

     $("#forgotPassword").click(function () {
        $('#forgot_username_form').hide();
        $('#change_password_form').hide();
        $('#forgot_password_form').show();
    });

     $("#changePassword").click(function () {
        $('#myModal').css('display', 'block');
        $('#forgot_password_form').hide();
        $('#forgot_username_form').hide();
        $('#formbox2').hide();
        $('#change_password_form').show();
    });

    $(".close").click(function (){
        $('#forgot_password_form').hide();
        $('#forgot_username_form').hide();
        $('#myModal').css('display', 'none');
    });

    $("#myBtn").click(function (){
        $('#myModal').css('display', 'block');
    });


    /*  --------   NAV START  ------- */

    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }

    /*  --------   DATA TABLE START  ------- */

    function format ( d ) {
        var cell = '';
        console.log(d);
//        if (d.links){
 //       cell += d.links.link(d.links);
  //      cell += "<br />";
   //     }

        cell += "Info Link: <a href=" + d.info_link + " target='_blank'>" + d.info_link + "</a><br />"
        cell += "Type: " + d.type + "<br />"
        cell += "<small><a href='/view/" + d._id.$oid + "'>" + d.comments.length + " comments  </a></small>"

        if ($('#auth').text() != 'None'){
            cell += "<small><a href='/edit/" + d._id.$oid + "' >  edit  </a></small>"
            cell += "<small><a href='/delete/" + d._id.$oid + "' >  delete  </a></small>"
        }

        if (d.active == false){
            cell += "<small><a href='/approve/" + d._id.$oid + "' >  approve  </a></small>"
        }
        console.log(cell);
        return cell;
//        return 'ID: '+d._id.$oid+'<br>'+
//            'State: '+d.active+'<br>'
    }

    function format_url ( d ) {
        return '/tables/' + d;
    }

    var dt = $('#example').DataTable( {

        "processing": true,
        "ajax": format_url($('#view').text()),
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            {
                "data": "counter",
                "width": "15%",
                "render": function ( data, type, row ) {

                    return '<a href="#" class="vote_button_up" id="' + row._id.$oid  +'_up"></a>  '
                    +  ' <span class="vote_number" id="' + row._id.$oid + '">' + data + '</span>'
                    + '  <a href="#" class="vote_button_down" id="' + row._id.$oid  +'_dn"></a>  ';
                },
            },
            { "data": "name",
              "render": function ( data, type, row ) {
                return '<a href="/view/' + row._id.$oid + '">' + data;
                }

             },

            { "data": "reason" }
        ],
        "order": [[1, 'desc']]
    } );

    // Array to track the ids of the details displayed rows
    var detailRows = [];

    $('#example tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );


/*  --------   DATATABLE END  ------- */


// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
});

$(document).on('click', "a.vote_button_up, a.vote_button_down", function() {
    var values = $(this).attr('id').split('_');
         event.preventDefault();
         $.ajax({
            url: '/vote',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(values),
            type: 'POST',
            success: function(response) {
                if (response.success){
                    console.log('update vote');
                    $('#' + response.id).html(response.data);
                } else {
                    console.log('error in vote');
                    console.log(response.message)
                    $('.user_error').text(response.message);
                }
            }
        });
});
