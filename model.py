class CoffeeShop:

    def __init__(self, name, street, city, state, zipcode, latitude, longitude):
        self.name = name
        self.street = street
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.latitude = latitude
        self.longitude = longitude


class APIResponse:
    def __init__(self, id, status, message):
        self.id = id
        self.status = status
        self.message = message
