from flask import flash, render_template, redirect, make_response
from __init__ import *
from math import sin, cos, acos, radians
import json
import jsonpickle
from model import APIResponse

def shortest_distance(lat1, lon1, lat2, lon2):
    slat = radians(lat1)
    slon = radians(lon1)
    elat = radians(lat2)
    elon = radians(lon2)

    dist = 6371.01 * acos(sin(slat) * sin(elat) + cos(slat) * cos(elat) * cos(slon - elon))
    return dist


def closest(v):
    lowest_distance = shortest_distance(float(v['lat']), float(v['lon']), float(coffee_shops[1].latitude), float(coffee_shops[1].longitude))
    closest_coffee_shop = coffee_shops[1]
    closest_id = 1
    for key, value in coffee_shops.items():
        current_distance = shortest_distance(v['lat'], v['lon'], float(value.latitude), float(value.longitude))
#        print str(key) + ' ' + str(current_distance)

        if current_distance < lowest_distance:
            lowest_distance = current_distance
            closest_coffee_shop = value
            closest_id = key

    return closest_id, closest_coffee_shop


def find_closest_location_by_lat_long(source_lat, source_long):
    v = {'lat': source_lat, 'lon': source_long}
    key, value = closest(v)
    return key, value


# ------------------------------------------------------------------------------
# -------------------------------  UI Wrapper Functions --------------------------------
# ------------------------------------------------------------------------------

@application.route('/', methods=['GET', 'POST'])
def home():
    try:
        if request.method == 'POST':
            result = find_closest()
            response = json.loads(result.data)
            if result.status_code > 200:
                raise Exception('Invalid ID')
            flash("Closest Shop is:  " + jsonpickle.encode(response['message']), 'success')
            return render_template('index.html')
        return render_template('index.html')
    except Exception as e:
        logger.error('An error occurred: ' + e.message)
        flash(response['status'] + " : " + response['message'] + " : " + str(response['id']), 'error')
        return render_template('error.html'), 500


@application.route('/wrapper_create', methods=['GET', 'POST'])
def wrapper_create():
    try:
        if request.method == 'POST':
            result = create()
            response = json.loads(result.data)
            if result.status_code > 200:
                raise Exception('Invalid ID')
            flash(response['status'] + " : " + response['message'] + " : " + str(response['id']), 'success')
            return render_template('index.html')
        return render_template('add_edit.html')
    except Exception as e:
        logger.error('An error occurred: ' + e.message)
        flash(response['status'] + " : " + response['message'] + " : " + str(response['id']), 'error')
        return render_template('error.html'), 500


@application.route('/wrapper_read', methods=['POST'])
def wrapper_read():
    try:
        id = int(request.form['id'])
        result = read(id)
        if result.status_code > 200:
            raise Exception('Invalid ID')
        coffee_shop = json.loads(result.data)
        flash("Result: " + coffee_shop['name'] + ' --  Located at: ' + coffee_shop['street'] + ' --  Lat: '
              + str(coffee_shop['latitude']) + ' Long: ' + str(coffee_shop['longitude']), 'success')
        return redirect(request.referrer)
    except Exception as e:
        flash("Error: " + str(e.message), 'error')
        return redirect(request.referrer)


@application.route('/wrapper_delete', methods=['POST'])
def wrapper_delete():
    try:
        id = int(request.form['id'])
        result = delete(id)
        if result.status_code > 200:
            raise Exception('Invalid ID')
        response = json.loads(result.data)
        flash(response['status'] + " : " + response['message'] + " : " + str(response['id']), 'success')
        return redirect(request.referrer)
    except Exception as e:
        flash("Error: " + str(e.message), 'error')
        return redirect(request.referrer)

# ------------------------------------------------------------------------------
# -------------------------------  CRUD CORE  ---------------------------------------
# ------------------------------------------------------------------------------

@application.route('/create', methods=['POST'])
def create():
    try:
        name = request.form['name']
        address = request.form['address']
        latitude = request.form['latitude']
        longitude = request.form['longitude']
        next_id = max(coffee_shops, key=int) + 1
        coffee_shop = CoffeeShop(name, address, None, None, None, latitude, longitude)
        coffee_shops[next_id] = coffee_shop
        success = APIResponse(next_id, 'SUCCESS', 'Record created')
        response = make_response(jsonpickle.encode(success), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    except Exception as e:
        error = APIResponse(id, 'ERROR: ', str(e.message))
        response = make_response(jsonpickle.encode(error), 404)
        response.headers['Content-Type'] = 'application/json'
        return response


@application.route("/update/<id>", methods=['PUT'])
def update(id):
    try:
        coffee_shop = coffee_shops[int(id)]
        coffee_shop.name = request.form['name']
        coffee_shop.address = request.form['address']
        coffee_shop.latitude = request.form['latitude']
        coffee_shop.longitude = request.form['longitude']
        success = APIResponse(id, 'SUCCESS', 'Record updated')
        response = make_response(jsonpickle.encode(success), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    except Exception as e:
        error = APIResponse(id, 'ERROR: ', str(e.message))
        response = make_response(jsonpickle.encode(error), 404)
        response.headers['Content-Type'] = 'application/json'
        return response


@application.route("/read/<id>", methods=['GET'])
def read(id):
    try:
        coffee_shop = coffee_shops[int(id)]
        response = make_response(jsonpickle.encode(coffee_shop), 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    except Exception as e:
        error = APIResponse(id, 'ERROR', 'Id not valid: ' + str(e.message))
        response = make_response(jsonpickle.encode(error), 404)
        response.headers['Content-Type'] = 'application/json'
        return response


@application.route("/delete/<id>", methods=['DELETE'])
def delete(id):
    try:
        coffee_shops.pop(int(id))
        success = APIResponse(id, 'SUCCESS', 'Record deleted')
        response = make_response(jsonpickle.encode(success), 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    except Exception as e:
        error = APIResponse(id, 'ERROR', 'Id not valid: ' + str(e.message))
        response = make_response(jsonpickle.encode(error), 404)
        response.headers['Content-Type'] = 'application/json'
        return response


@application.route('/find_closest', methods=['POST'])
def find_closest():
    try:
        street = request.form['street']
        city = request.form['city']
        state = request.form['state']
        zipcode = request.form['zipcode']
        geocode_result = gmaps.geocode(street + ' ' + city + ' ' + state + ' ' + zipcode)

        key, closest_coffee_shop = find_closest_location_by_lat_long(geocode_result[0]['geometry']['location']['lat'], geocode_result[0]['geometry']['location']['lng'])

        success = APIResponse(key, 'SUCCESS', closest_coffee_shop)
        response = make_response(jsonpickle.encode(success), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    except Exception as e:
        error = APIResponse(0, 'ERROR: ', str(e.message))
        response = make_response(jsonpickle.encode(error), 404)
        response.headers['Content-Type'] = 'application/json'
        return response

# ------------------------------------------------------------------------------
# -------------------------------  SPECIAL  ------------------------------------
# ------------------------------------------------------------------------------


@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# ------------------------------------------------------------------------------
# ----------------------------------   MAIN   ----------------------------------
# ------------------------------------------------------------------------------

if __name__ == '__main__':

    if dev:
        application.jinja_env.globals['dev'] = True
        application.run()

    else:
        try:
            application.run()
        except Exception as e:
            logger.error('An error occurred: ' + e.message)
