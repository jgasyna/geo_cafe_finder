# Find Closest Coffee Shop

## Overview

This little project reads in an CSV of coffee shops and then exposes an API to find the closest coffee shop by address, along with basic CRUD.  It uses Google's Geo API. 
### Required PIP packages

- flask
- gevent
- googlemaps
- jsonpickle

### Deploy Instructions
- Please install PIP using favorite MAC package installed (Like BREW)
- It is recommended that you run this in a virtual environment.
- $ sudo pip install -r requirements.txt 
- $ python server.py
- The server runs on localhost:5000

## API Documentation:
####CREATE - Create a new coffee shop:
- Endpoint: /create  (POST)
###### BODY PARAMETERS
- name: string
- address: string
- latitude: string
- longitude: string

####UPDATE - Update a new coffee shop by ID:
- Endpoint:  /update/{id} (PUT)
###### BODY PARAMETERS
- name: string
- address: string
- latitude: string
- longitude: string

####READ - Fetch coffee shop by ID:
- Endpoint:  /read/{id}  (GET)

#### DELETE - Delete coffee shop by ID:
- Endpoint:  /delete/{id}  (DELETE)

####FIND CLOSEST -- Find closest coffee shop
- Endpoint:  /find_closest (POST)
###### BODY PARAMETERS
- street : string
- city  : string
- state  : string
- zipcode  : string



## Assumptions, Notes and TODO:
- Assumes Python 2.7
- You can use Postman to test the endpoints, but there is also a UI at localhost:5000
- Add more validation for data entry
- Better insert logic for id’s.  Ideally you would actually use Mongo for this, and not file a system, which makes id management a pain.
- For coffee shop creation, I left out the city, state and opcode for now.  However, these fields exist in the model.
- Only the REST operations mentioned are allowed (POST, GET etc)