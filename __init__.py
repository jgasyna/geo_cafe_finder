from flask import Flask, request
import os, sys
import logging
from logging.handlers import RotatingFileHandler
from model import CoffeeShop
import csv
import googlemaps

application = Flask(__name__, static_folder='static')
logger = logging.getLogger()
handler = RotatingFileHandler('error.log', maxBytes=10000, backupCount=1)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

dev = False
if os.environ.has_key('DEV'):
    dev = True
    logging.getLogger().addHandler(logging.StreamHandler())
    logging.getLogger().setLevel(logging.INFO)

application.config["TEMPLATES_AUTO_RELOAD"] = True
application.config["SECRET_KEY"] = "N0MORESECRETS09877654321"


logger.info('Loading Data Set....')

try:
    with open('locations.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        coffee_shops = dict()
        for row in csv_reader:
            input_coffee_shop = CoffeeShop(row[1], row[2], 'San Francisco', 'CA', '90210', row[3], row[4])
            coffee_shops[int(row[0])] = input_coffee_shop

    logger.info('Found: ' + str(len(coffee_shops)) + ' coffee shops in initial data set')

    gmaps = googlemaps.Client(key='AIzaSyArcGRSh_JBsZ1zXnu8yEyfSwZMVtcH53E')

except Exception as e:
    logger.error(e.message)
